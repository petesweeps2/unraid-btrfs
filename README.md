# HOW TO: Create SMB Share on UnRaid that replicates to a btrfs disk and uploads to pCloud (aka Janky Ransomware Protection)

The goal of this exercise is to do the following:
- Create a SMB/NFS Share that you can backup your critical files
- Replicate said share to a btrfs disk that has snapshot capabilities
- Upload replicated data to pCloud
- Shutdown access to share if mass file rename and/or deletes are detected

## The following Plugins are required:
- Community Applications
https://forums.unraid.net/topic/38582-plug-in-community-applications/

- CA User Scripts
https://forums.unraid.net/topic/48286-plugin-ca-user-scripts/

- rClone
https://forums.unraid.net/topic/51633-plugin-rclone/

- Unassigned Devices
https://forums.unraid.net/topic/92462-unassigned-devices-managing-disk-drives-and-remote-shares-outside-of-the-unraid-array/

- If new, follow these steps on how to install Plugins:
https://www.youtube.com/watch?v=CXhIQSwJbTA

## Required equipment
- Unraid Server
- Spare HDD that is not part of your unraid array (I would go with 2TB or larger depending on your needs)
- pCloud Account: Feel free to use my referral https://www.pcloud.com/welcome-to-pcloud/?discountcode=3H6980gJbGXyHpeGlUGoU0ZV&locationid=1)

----

## Share Setup (Easy)
Click on Shares -> Add Shares and setup a new share, options are not important, use whatever you want
 ![](/img/1.jpg)

----

## BTFRS Setup (Hard)
- Steps stolen from https://forums.unraid.net/topic/32162-unraid-6-beta-6-btrfs-quick-start-guide/

### Add your disk to your Unraid Server, Click on Main and navigate down to Unassigned Devices, there you can mount your disk you will be using for BTRFS
![](/img/2.jpg)

### Click the Device Link to get the device id
![](/img/3.jpg)

### Note the Device Identifier (in my case it is sdb)
![](/img/4.jpg)

### Once the disk is visible and mounted, open a SSH (Console) Session to the Unraid Server
![](/img/5.jpg)

### Use lsblk to examine the btrfs disk (why? because we can)
![](/img/6.jpg)

### If there is a partition on your btrfs disk, use fdisk to wipe it using the following commands (MAKE SURE USE USE THE RIGHT /dev/disk id, if you wipe the wrong disk, it will be a disaster)
```sh
   fdisk /dev/sdb
   d
   w
   q
```

### Create a new Partitoin on the btrfs disk
```sh
   sgdisk -g -N 1 /dev/sdb
```

### lsblk will now list the partition
![](/img/6.jpg)

### Format the disk partition with btrfs (substitute sdb1 with whatever lsblk shows you for the disk id and paritition number)
```sh
  mkfs.btrfs -f /dev/sdb1
```

### Make a Mount Point
```sh
  mkdir /mnt/btrfs
  mount /dev/sdb1 /mnt/btrfs
```

### Get the disk by id
```sh
  v /dev/disk/by-id
```
![](/img/7.jpg)

### Modify /boot/config/go and add the following (change ata-WDC_WD30EFRX-68EUZN0_WD-WMC4N2900433-part1 to what you got from the prior command).  For those who don't know how to use VI, the command would basically be:
#### Highlight and Copy the below lines (with your changes)
```sh
 vi /boot/config/go
 Shift+G
 o
 Shift+Insert
 ESC
 :wq
```

```sh
mount /dev/disk/by-id/ata-WDC_WD30EFRX-68EUZN0_WD-WMC4N2900433-part1 /mnt/btrfs
rm -rf /mnt/user/BTRFS
ln -s /mnt/btrfs /mnt/user/BTRFS
```

### Create a subvolume for the .snapshots and your replicated share data
```sh
  btrfs subvolume create /mnt/btrfs/.snapshots
  btrfs subvolume create /mnt/btrfs/BACKUPS
```

### Make a share for btrfs so you can easily see through wnidows whats in the BACKUPS and .snapshots directories
1. In Shares, make a new share called btrfs
2. Set the permissions to secure so that its not writeable through the network (this is a ransomware preventivative measure)
3. Go to shell, and run:
```sh
rm -rf /mnt/users/btrfs
```
4. Make a symlink to the real location
```sh
ln -s /mnt/btrfs /mnt/users/btrfs
```

### Extra Stuff If Needed
#### How to Manually Create Snapshot:
```sh
  btrfs subvolume snapshot -r "/mnt/btrfs/BACKUPS" "/mnt/btrfs/.snapshots/<name>"
```

#### How to Restore Snapshot:
- Stop scripts schedule
- Delete the infected BACKUP BTRFS Volume
```sh
btrfs subvolume delete /mnt/btrfs/BACKUPS
```
- Restore the volume from a snapshot
```sh
btrfs subvolume snapshot "/mnt/btrfs/.snapshots/hourly_2021-02-18_125315" "/mnt/btrfs/BACKUPS"
```

## pCloud - rClone Setup (Hard)
https://rclone.org/pcloud/

### Since this is a headless installation, you will need to install rclone on a windows system as well in order to generate the token.  Install rclond for windows from here:
https://rclone.org/downloads/

### On Windows, open a command prompt, navigate to the rclone files that were extracted and run the following
  ```sh
  rclone authorize "pcloud"
  ```
  ![](/img/8.jpg)

### Your browser will take you to pCloud where you will need to login
  ![](/img/9.jpg)

### After you login, you will get a Success Message
  ![](/img/10.jpg)

### Your command prompt will now have the token required to configure rclone for unraid
```sh
  Paste the following into your remote machine --->
    {"access_token":"<a long token>","token_type":"bearer","expiry":"0001-01-01T00:00:00Z"}
  <---End paste
```

### Back at Unraid Console, run the following  
```sh
  rclone config
  n
  BACKUPS
  pcloud
  ENTER
  ENTER
  n
  n
```

### Paste your token in
  ![](/img/11.jpg)
#### Then Yes  
```sh
  y
```
### you should now see your pcloud endpoint

![](/img/12.jpg)

## Almost Done!  All the major work is now done, all we need to do is load up some scripts to do the real work.

## Download the following 6 scripts from here

https://gitlab.com/petesweeps2/unraid-btrfs

### In Unraid UI, go to Settings -> User Scripts

### Click Add New Script

![](/img/13.jpg)

### Mouse over the Gear next to the script and select Edit Script
![](/img/14.jpg)

### Delete the default line
```sh
#!/bin/bash
```

### Paste the contents of the script then Save Changes
![](/img/15.jpg)

### Repeat for all 6 scripts, when done, set the following schedules
| Script          | Schedule                  | Run in Background |
|-----------------|---------------------------|-------------------|
| btrfs-snp.sh    | Schedule Disabled         | No                |
| mount-btrfs.sh  | At First Array Start Only | No                |
| pcloudsync.sh   | At Startup of Array       | Yes               |
| ransomwatch.sh  | At Startup of Array       | Yes               |
| rsync.sh        | At Startup of Array       | Yes               |
| snapshots.sh    | Scheduled Hourly          | No                |

Last but not least .... Fuck off Hugh!
