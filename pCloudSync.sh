#!/bin/bash

while true; do
 inotifywait -r /mnt/btrfs/BACKUPS
 date
 echo "Running sync at 2MB/s"
 rclone sync --bwlimit 2000 /mnt/btrfs/BACKUPS pCloud:BACKUPS -P
done
