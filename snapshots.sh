#!/bin/bash
/bin/bash /boot/config/plugins/user.scripts/scripts/btrfs-snp/script /mnt/btrfs/BACKUPS hourly  24 3600
/bin/bash /boot/config/plugins/user.scripts/scripts/btrfs-snp/script /mnt/btrfs/BACKUPS daily    7 86400
/bin/bash /boot/config/plugins/user.scripts/scripts/btrfs-snp/script /mnt/btrfs/BACKUPS weekly   4 604800
/bin/bash /boot/config/plugins/user.scripts/scripts/btrfs-snp/script /mnt/btrfs/BACKUPS monthly 12 2592000
