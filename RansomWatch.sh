#!/bin/bash
WATCHPATH="/mnt/user/BACKUPS"
COUNTER=0
SUBCOUNTER=0
SUBTRIP=5 # Number of Main Loop trips before entering in for threat detection
THREATTIMELIMIT=1000 # Threat Time Limit in miliseconds
THREATCOUNTLIMIT=10 # Number of events that are allowed within THREATTIMELIMIT before triggering alarms 

declare -A cache
inotifywait  -r --format="%c %e %f" -m -e move $WATCHPATH |
while read id event file; do
    SUBSTART=`date +%s%3N`
	let COUNTER++
    if [ "$event" = "MOVED_FROM" ]; then
        cache[$id]=$file
    fi
    if [ "$event" = "MOVED_TO" ]; then
        if [ "${cache[$id]}" ]; then
		    # Start of Processing
            echo "Processing Main Loop..."
			
			# On the SUBTRIP (5) Move File, we are going to enter into our subloop to check if move files are happening too quickly
			if (( COUNTER > SUBTRIP )); then
				declare -A subcache
				inotifywait  --format="%c %e %f" -m -e move $WATCHPATH |
				while read subid subevent subfile; do
					
					if [ "$subevent" = "MOVED_FROM" ]; then
						subcache[$subid]=$subfile
					fi
					if [ "$subevent" = "MOVED_TO" ]; then
						if [ "${subcache[$subid]}" ]; then
							# Start of Sub-Processing
							echo "Processing Sub Loop..."
							SUBDATE=`date +%s%3N`
							let SUBCOUNTER++
							
							TEST=$(($SUBDATE - $SUBSTART))
							echo "SUBCOUNTER: $SUBCOUNTER : THREATCOUNTLIMIT: $THREATCOUNTLIMIT"
							echo "TEST: $TEST THREATTIMELIMIT: $THREATTIMELIMIT"
							
							if [[ "$SUBCOUNTER" -ge "$THREATCOUNTLIMIT" ]]; then
							  if [[ "$TEST" -le "$THREATTIMELIMIT" ]]; then
								echo "WE NOT OK TRIGGER WARNING"
								/etc/rc.d/rc.samba stop
							  else
								echo "WE OK, heading back to main loop"
								COUNTER=0
								SUBCOUNTER=0
								break;
							  fi
							 
							fi
														
							# Sub-Processing has Ended
							unset subcache[$subid]
						else
							echo "mismatch for $subid"
						fi
					fi
				done
			fi
			
			
			# Processing has Ended
            unset cache[$id]
        else
            echo "mismatch for $id"
        fi
    fi
done
